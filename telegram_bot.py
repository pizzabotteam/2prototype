# coding=utf-8
import sys
import time
import telepot
import os
import datetime
import threading
from threading import Lock

import requests
import commentjson
from urlparse import urljoin

TOKEN = sys.argv[1]
VINS_URL = u''
API_KEY_ID = u''
CLIENT_TOKEN =u''
http = requests.session()
FORMS = {}
START_Q = "Чтобы сделать заказ просто напишите 'Хочу сделать заказ'!"
ITEMS = ["пицца", "напиток"]

with open("Vinsfile.js", "r") as f:
    cfg = commentjson.load(f)
    CLIENT_TOKEN = cfg['tokens'].get('client')
    VINS_URL = cfg.get('vins_url')
    API_KEY_ID = cfg.get('app_id')

print(CLIENT_TOKEN)
print(VINS_URL)
print(API_KEY_ID)


def request(uuid, utterance):
    data = {
        "utterance": utterance
    }
    json_data = commentjson.dumps(
        data,
        ensure_ascii=False
    ).encode('utf-8')

    params = {
        'lang': 'ru-Ru',
        'uuid': uuid,
        'debug': 1
    }
    headers = {
        "x-vins-client-token": CLIENT_TOKEN
    }

    r = http.post(
        urljoin(VINS_URL, '//v0.1/vins/client/'),
        params=params, data=json_data, headers=headers
    )
    if not r.ok:
        raise Exception('Failed request to "{0}" with code {1}'.format(r.url, r.status_code))
    else:
        pass
    return r.json()

def vins_response_handle (nextStep, vins_commands, uuid):
    currentStep = FORMS[uuid]["step"]
    if currentStep == None:
        if nextStep == "add":
            FORMS[uuid]["step"] = "add"
            FORMS[uuid]["q"] = vins_commands[2]['text']
            return [vins_commands[2]['text']]
        elif nextStep == "item":
            FORMS[uuid]["step"] = "item"
            FORMS[uuid]["q"] = vins_commands[3]['text']
            FORMS[uuid]["items"].append(int(vins_commands[4]['text']))
            return [vins_commands[2]['text'], vins_commands[3]['text']]

    elif currentStep == "add":
        if nextStep == "item":
            FORMS[uuid]["step"] = "item"
            FORMS[uuid]["q"] = vins_commands[3]['text']
            FORMS[uuid]["items"].append(int(vins_commands[4]['text']))
            return [vins_commands[2]['text'], vins_commands[3]['text']]

    elif currentStep == "item":
        if nextStep == "confirm":
            FORMS[uuid]["step"] = "not_all"
            FORMS[uuid]["q"] = vins_commands[3]['text']
            return [vins_commands[2]['text'], vins_commands[3]['text']]
        elif nextStep == "not_confirm":
            FORMS[uuid]["step"] = "add"
            FORMS[uuid]["items"].pop()
            FORMS[uuid]["q"] = vins_commands[3]['text']
            return [vins_commands[2]['text'], vins_commands[3]['text']]

    elif currentStep == "not_all":
        if nextStep == "confirm":
            FORMS[uuid]["step"] = "add"
            FORMS[uuid]["q"] = vins_commands[4]['text']
            return [vins_commands[4]['text']]
        elif nextStep == "not_confirm":
            FORMS[uuid]["step"] = "check_order"
            ans = vins_commands[4]['text'] + " "
            for item in FORMS[uuid]["items"]:
                ans += "\n" + ITEMS[item]
            FORMS[uuid]["q"] = vins_commands[5]['text']
            return [ans, vins_commands[5]['text']]

    elif currentStep == "check_order":
        if nextStep == "confirm":
            FORMS[uuid]["step"] = None
            FORMS[uuid]["q"] = START_Q
            return [vins_commands[5]['text']]
        elif nextStep == "not_confirm":
            FORMS[uuid]["step"] = None
            FORMS[uuid]["q"] = START_Q
            return [vins_commands[6]['text']]

    if nextStep == "menu":
        ans = vins_commands[2]['text'] + " "
        for item in ITEMS:
            ans += "\n" + item
        return [ans]

    return [vins_commands[1]['text'], [FORMS[uuid]["q"]]]


def execute(msg, uuid):
    try:
        answer = request(uuid, msg)
    except Exception as e:
        print('Error: {0}'.format(e))
        bot.sendMessage(uuid, "При обработке запроса произошла ошибка.")
    else:
        if not FORMS.has_key(uuid):
            FORMS[uuid] = {}
            FORMS[uuid]["step"] = None
            FORMS[uuid]["q"] = START_Q
        commands = []
        for cmd in answer['commands']:
            if cmd['command'] == 'prompt':
                commands.append(cmd)
        responses = vins_response_handle(commands[0], commands, uuid)
        for resp in responses:
            bot.sendMessage(uuid, resp)
    #bot.sendMessage(chat_id,"")
    #bot.sendChatAction(chat_id, 'upload_photo')


def handle(msg):
    global IsNotUse
    uuid = msg['chat']['id']
    textIsBe = msg.get('text', "None")
    if textIsBe != "None":
        command = msg['text']
        t = threading.Thread(target=execute, args=(command, uuid))
        t.daemon = True
        t.start()


bot = telepot.Bot(TOKEN)
bot.message_loop(handle)

# Keep the program running.
while 1:
    time.sleep(10)