#!/usr/bin/env python
# vim: set fileencoding=utf-8

import json
import logging
import random
import os
import sys
import requests
import commentjson
from urlparse import urljoin
import click

from flask import Flask
from flask import request

application = Flask(__name__)

db = {}

items = [u'пицца', u'напиток']

# --- DEFAULT HANDLER ---


def default_handler(api_handler):
    try:
        # --- parsing request ---

        form = request.get_json()
        if not form:
            raise Exception('form is missing')

        logging.debug(
            'form: %s' % json.dumps(form, indent=2, ensure_ascii=False)
        )

        uuid = request.args.get('uuid')
        if not uuid:
            raise Exception('uuid is missing')

        # --- handling ---

        # TODO: Workround.
        # InvalidDocument: key u'requirements.smoking' must not contain '.'
        for slot, value in form.items():
            if slot.find('.'):
                del form[slot]
                slot = slot.replace('.', '__')
                form[slot] = value

        global db
        response = api_handler(db, form, uuid)

        for slot, value in form.items():
            if slot.find('__'):
                del form[slot]
                slot = slot.replace('__', '.')
                form[slot] = value

        # --- result ---

        response = json.dumps(response, indent=2, ensure_ascii=False)

        logging.debug("response: %s" % response)

        return response, 200, {
            'Access-Control-Allow-Origin': '*',
            'X-Content-Type-Options': 'nosniff',
            'Content-Type': 'application/json'
        }

    except Exception as exc:
        logging.exception(exc)
        return "Server error", 500, {
            'Access-Control-Allow-Origin': '*',
            'X-Content-Type-Options': 'nosniff',
            'Content-Type': 'text/plain'
        }

def item_handler(api_handler, param):
    try:
        # --- parsing request ---

        form = request.get_json()
        if not form:
            raise Exception('form is missing')

        logging.debug(
            'form: %s' % json.dumps(form, indent=2, ensure_ascii=False)
        )

        uuid = request.args.get('uuid')
        if not uuid:
            raise Exception('uuid is missing')

        # --- handling ---

        # TODO: Workround.
        # InvalidDocument: key u'requirements.smoking' must not contain '.'
        for slot, value in form.items():
            if slot.find('.'):
                del form[slot]
                slot = slot.replace('.', '__')
                form[slot] = value

        global db
        response = api_handler(db, form, uuid, param)

        for slot, value in form.items():
            if slot.find('__'):
                del form[slot]
                slot = slot.replace('__', '.')
                form[slot] = value

        # --- result ---

        response = json.dumps(response, indent=2, ensure_ascii=False)

        logging.debug("response: %s" % response)

        return response, 200, {
            'Access-Control-Allow-Origin': '*',
            'X-Content-Type-Options': 'nosniff',
            'Content-Type': 'application/json'
        }

    except Exception as exc:
        logging.exception(exc)
        return "Server error", 500, {
            'Access-Control-Allow-Origin': '*',
            'X-Content-Type-Options': 'nosniff',
            'Content-Type': 'text/plain'
        }

# --- HANDLERS ---


@application.route('/show_items', methods=["POST", "GET"])
def show_items():
    return default_handler(api_show_items)

@application.route('/item/<int:item_id>', methods=["POST", "GET"])
def item(item_id):
    return item_handler(api_add_item, item_id)

@application.route('/add_item/<int:item_id>', methods=["POST", "GET"])
def add_item(item_id):
    return item_handler(api_confirm_add_item, item_id)

@application.route('/check_all_item', methods=["POST", "GET"])
def check_all_item():
    return default_handler(api_check_all_item)

@application.route('/all_item', methods=["POST", "GET"])
def all_item():
    return default_handler(api_all_item)

@application.route('/order', methods=["POST", "GET"])
def complete_order():
    return default_handler(api_complete_order)


# --- API ---

def api_all_item(db, form, uuid):
    is_all = int(form['all_items'])
    if (is_all):
        del form['all_items']
        del form['start_order']
        return {
            'actions': [{
                'action': 'nlg',
                'id': 'ask__start_order',
            	'form': form
        	}]
            }
    else:
        form['confirm'] = True
    	return {
            'actions': [{
                'action': 'nlg',
                'id': 'ask__items_done',
            	'form': form
        	}]
            }

def api_check_all_item(db, form, uuid):
    confirm = bool(form['add_item'])
    if confirm:
        first_item = form['first_item']
        if first_item is None:
            form['first_item'] = True
        confirm = bool(form['confirm_add_item'])
        if (confirm):
            return {
                'actions': [{
                    'action': 'nlg',
                    'id': 'show_item_info',
                	'form': form
            	}]
            }
        else:
        	return {
                'actions': [{
                    'action': 'nlg',
                    'id': 'show_item_info',
                	'form': form
            	}]
            }
    else:
        first_item = form['first_item']
        if first_item is None:
            return {
                'actions': [{
                    'action': 'nlg',
                    'id': 'ask__start_order',
                	'form': form
            	}]
            }
        else:
        	return {
                'actions': [{
                    'action': 'nlg',
                    'id': 'ask__confirm_all',
                	'form': form
            	}]
            }

def api_add_item(db, form, uuid, item_id):
    form['item_info'] = items[item_id]
    form['start_order'] = u'start'
    form['item_id'] = item_id
    click.secho(str(form))
    return {
        'actions': [{
            'action': 'nlg',
            'id': 'show_item_info',
            'form': form
        }]
    }

def api_confirm_add_item(db, form, uuid, item_id):
    confirm = bool(form['confirm_add_item'])
    if (not db.has_key(uuid)):
        db[uuid] = []
    db[uuid].append(int(item_id))
    click.secho("last" + str(form))
    form['_active_slots_'] = [u'start_order']
    form['item_info'] = items[item_id]
    click.secho("now" + str(form))
    if (confirm):
        return {
            'actions': [{
                'action': 'nlg',
                'id': 'ask__confirm_add_item',
            	'form': form
        	}]
            }
    else:
    	return {
            'actions': [{
                'action': 'nlg',
                'id': 'ask__non_confirm_add_item',
            	'form': form
        	}]
            }

def api_show_items(db, form, uuid):
    form['items'] = (', ').join(items)
    return {
        'actions': [{
            'action': 'nlg',
            'id': 'show_all_items',
            'form': form
        }]
    }

def api_complete_order(db, form, uuid):
    return {
        'actions': [{
            'action': 'nlg',
            'id': 'ask__order_is_completed',
            'form': form
        }]
    }

@application.route('/ping')
def ping():
    return "1", 200, {'Content-Type': 'text/plain'}

def say_to_vins(message, uuid):
    data = {
        "utterance": message
    }
    json_data = commentjson.dumps(
        data,
        ensure_ascii=False
    ).encode('utf-8')

    params = {
        'lang': 'ru-Ru',
        'uuid': uuid,
        'debug': 1
    }

    r = http.post(
        urljoin(url, '/v0.1/vins/client/'),
        params=params, data=json_data
    )
    return r.ok


http = requests.session()
url = "https://vinslabs.tst.mobile.yandex.net/1.0/kkxnSQQax324kPnW1Wclaekwnklxw"
if __name__ == '__main__':
    application.run(host="localhost", port=7777, debug=True)
