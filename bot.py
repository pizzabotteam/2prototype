# coding: utf-8

from __future__ import unicode_literals

import click
import requests
import commentjson
from telegram.ext import Updater, CommandHandler, MessageHandler
from urlparse import urljoin
from vins_cli.exceptions import VinsCfgParseError
import sys

bot_token = sys.argv[1]
VINS_URL = u''
API_KEY_ID = u''
CLIENT_TOKEN =u''
http = requests.session()
FORMS = {}
START_Q = "Чтобы сделать заказ просто напишите 'Хочу сделать заказ'!"
ITEMS = ["пицца", "напиток"]


def vins_response_handle (nextStep, vins_commands, uuid):
    global FORMS
    global ITEMS
    currentStep = FORMS[uuid]["step"]
    if currentStep == None:
        if nextStep == "add":
            FORMS[uuid]["step"] = "add"
            FORMS[uuid]["q"] = vins_commands[2]['text']
            return [vins_commands[2]['text']]
        elif nextStep == "item":
            FORMS[uuid]["step"] = "item"
            FORMS[uuid]["q"] = vins_commands[3]['text']
            FORMS[uuid]["items"].append(int(vins_commands[4]['text']))
            item_name = ITEMS[int(vins_commands[4]['text'])]
            return [vins_commands[2]['text'] + " " + item_name, vins_commands[3]['text']]

    elif currentStep == "add":
        if nextStep == "item":
            FORMS[uuid]["step"] = "item"
            FORMS[uuid]["q"] = vins_commands[3]['text']
            FORMS[uuid]["items"].append(int(vins_commands[4]['text']))
            item_name = ITEMS[int(vins_commands[4]['text'])]
            return [vins_commands[2]['text'] + " " + item_name, vins_commands[3]['text']]

    elif currentStep == "item":
        if nextStep == "confirm":
            FORMS[uuid]["step"] = "not_all"
            FORMS[uuid]["q"] = vins_commands[3]['text']
            return [vins_commands[2]['text'], vins_commands[3]['text']]
        elif nextStep == "not_confirm":
            FORMS[uuid]["step"] = "add"
            FORMS[uuid]["items"].pop()
            FORMS[uuid]["q"] = vins_commands[3]['text']
            return [vins_commands[2]['text'], vins_commands[3]['text']]

    elif currentStep == "not_all":
        if nextStep == "confirm":
            FORMS[uuid]["step"] = "add"
            FORMS[uuid]["q"] = vins_commands[4]['text']
            return [vins_commands[4]['text']]
        elif nextStep == "not_confirm":
            FORMS[uuid]["step"] = "check_order"
            ans = vins_commands[4]['text'] + " "
            for item in FORMS[uuid]["items"]:
                ans += "\n" + ITEMS[item]
            FORMS[uuid]["q"] = vins_commands[5]['text']
            return [ans, vins_commands[5]['text']]
        elif nextStep == "item":
            FORMS[uuid]["step"] = "item"
            FORMS[uuid]["q"] = vins_commands[3]['text']
            FORMS[uuid]["items"].append(int(vins_commands[4]['text']))
            item_name = ITEMS[int(vins_commands[4]['text'])]
            return [vins_commands[2]['text'] + " " + item_name, vins_commands[3]['text']]

    elif currentStep == "check_order":
        if nextStep == "confirm":
            FORMS[uuid]["step"] = None
            FORMS[uuid]["q"] = START_Q
            return [vins_commands[5]['text']]
        elif nextStep == "not_confirm":
            FORMS[uuid]["step"] = None
            FORMS[uuid]["q"] = START_Q
            return [vins_commands[6]['text']]

    if nextStep == "menu":
        ans = vins_commands[2]['text'] + " "
        for item in ITEMS:
            ans += "\n" + item
        return [ans]

    return [vins_commands[1]['text'], FORMS[uuid]["q"]]

class VinsAPI:
    def __init__(self, vins_url, app_id, token_client):
        self.url = vins_url
        self.app_id = app_id
        self.token = token_client
        self.http = requests.session()

    def request(self, uuid, utterance):
        data = {
            "utterance": utterance
        }
        json_data = commentjson.dumps(
            data,
            ensure_ascii=False
        ).encode('utf-8')

        params = {
            'lang': 'ru-Ru',
            'uuid': uuid,
            'debug': 1
        }
        headers = {
            "x-vins-client-token": self.token
        }

        r = self.http.post(
            urljoin(self.url, '/v0.1/vins/client/'),
            params=params, data=json_data, headers=headers
        )
        if not r.ok:
            raise Exception('Failed request to "{0}" with code {1}'.format(r.url, r.status_code))
        else:
            pass
            # click.echo('url: {0} request: {1} data: {2} response: {3}'.format(r.url, params, json_data, r.content)
        return r.json()

    def bot_handler(self, bot, update):
        utterance = update.message.text
        if not utterance:
            return
        click.secho('<= {}'.format(utterance), fg='green')
        try:
            answer = self.request(update.message.chat_id, utterance)
        except Exception as e:
            click.secho('Error: {0}'.format(e), fg='red')
        else:
            global FORMS
            uuid = update.message.chat_id
            if not FORMS.has_key(uuid):
                FORMS[uuid] = {}
                FORMS[uuid]["step"] = None
                FORMS[uuid]["q"] = START_Q
                FORMS[uuid]["items"] = []
            commands = []
            for cmd in answer['commands']:
                if cmd['command'] == 'prompt':
                    commands.append(cmd)
            try:
                responses = vins_response_handle(commands[0]['text'], commands, uuid)
                for resp in responses:
                    click.secho('=> {}'.format(resp), fg='blue')
                    bot.sendMessage(update.message.chat_id, text=resp)
            except:
                for cmd in answer['commands']:
                    if cmd['command'] == 'prompt':
                        click.secho('=> {}'.format(cmd['text']), fg='blue')
                        bot.sendMessage(update.message.chat_id, text=cmd['text'])


def start(bot, update):
    bot.sendMessage(update.message.chat_id, text='Привет!')


def error(bot, update, error):
    click.secho('Update "{0}" caused error "{1}"'.format(update, error), fg='red')


def run_bot(bot_token, vins_app_id, vins_url, vins_client_token):
    vins_api = VinsAPI(vins_url, vins_app_id, vins_client_token)

    # Create the EventHandler and pass it your bot's token.
    updater = Updater(bot_token, job_queue_tick_interval=0.1, workers=1)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler([lambda x: True], vins_api.bot_handler))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    click.secho('Bot started')
    # Run the bot until the you presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()

with open("Vinsfile.js", "r") as f:
    cfg = commentjson.load(f)
    # - checking app section -
    if cfg.get('tokens') is None:
        raise VinsCfgParseError('"tokens" section is missing')
    token_client = cfg['tokens'].get('client')
    vins_url = cfg.get('vins_url')
    app_id = cfg.get('app_id')
    if token_client is None:
        raise VinsCfgParseError('"client token" is missing')
    if vins_url is None:
        raise VinsCfgParseError('"vins_url" is missing')
    if app_id is None:
        raise VinsCfgParseError('"app_id" is missing')

    run_bot(bot_token, app_id, vins_url + '/', token_client)