import sys
import commentjson
import os
import click
import requests
import files
from urlparse import urljoin

VINS_URL = u'https://vinslabs.tst.mobile.yandex.net/1.0/kkxnSQQax324kPnW1Wclaekwnklxw'
API_KEY = sys.argv[1]

def register_app(api_key, vins_url):
    # type: (object, object) -> object
    headers = {
        "x-vins-api-key": api_key
    }
    r = requests.get(urljoin(vins_url, "/v0.1/dm/reg_app/"), headers=headers)
    if not r.ok:
        raise VinsCfgParseError(
            'application registration failed' +
            ' (http error code: %d) !' % r.status_code,
        )

    return r.json()

click.secho('Registering...', fg='blue')
js = register_app(API_KEY, VINS_URL)
click.secho('Making Vinsfile...', fg='blue')
cfg = DEFAULT_VINS_CFG_FILE_TPL % dict(
        app_id=js['app_id'],
        developer_token=js['developer_token'],
        client_token=js['client_token'],
        api_key=api_key,
        vins_url=vins_url
    )
cfg = commentjson.loads(cfg)

with open("Vinsfile.js", "w") as f:
    commentjson.dump(cfg, f, indent=2, ensure_ascii=False)

os.mkdir('general')
os.mkdir('general/intents')



