# vim: set fileencoding=utf-8

DEFAULT_VINS_CFG_FILE_TPL = """
// your comment here

{
  "vins_url": "%(vins_url)s",
  "api_key": "%(api_key)s",
  "tokens": {
    "developer": "%(developer_token)s",
    "client": "%(client_token)s"
  },
  "app_id": "%(app_id)s",
  "project": {
    "name": "mybot",
    "includes": [{
      "type": "file",
      "path": "general/VinsProjectfile.js"
    }]
  }
}
"""