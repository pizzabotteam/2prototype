# coding: utf-8

from __future__ import unicode_literals

import click
import requests
import commentjson
from telegram.ext import Updater, CommandHandler, MessageHandler
from urlparse import urljoin
from vins_cli.exceptions import VinsCfgParseError


class VinsAPI:
    def __init__(self, vins_url, app_id, token_client):
        self.url = vins_url
        self.app_id = app_id
        self.token = token_client
        self.http = requests.session()

    def request(self, uuid, utterance):
        data = {
            "utterance": utterance
        }
        json_data = commentjson.dumps(
            data,
            ensure_ascii=False
        ).encode('utf-8')

        params = {
            'lang': 'ru-Ru',
            'uuid': uuid,
            'debug': 1
        }
        headers = {
            "x-vins-client-token": self.token
        }

        r = self.http.post(
            urljoin(self.url, '/v0.1/vins/client/'),
            params=params, data=json_data, headers=headers
        )
        if not r.ok:
            raise Exception('Failed request to "{0}" with code {1}'.format(r.url, r.status_code))
        else:
            pass
            # click.echo('url: {0} request: {1} data: {2} response: {3}'.format(r.url, params, json_data, r.content)
        return r.json()

    def bot_handler(self, bot, update):
        utterance = update.message.text
        if not utterance:
            return
        click.secho('<= {}'.format(utterance), fg='green')
        try:
            answer = self.request(update.message.chat_id, utterance)
        except Exception as e:
            click.secho('Error: {0}'.format(e), fg='red')
        else:
            for cmd in answer['commands']:
                if cmd['command'] == 'prompt':
                    click.secho('=> {}'.format(cmd['text']), fg='blue')
                    bot.sendMessage(update.message.chat_id, text=cmd['text'])


def start(bot, update):
    bot.sendMessage(update.message.chat_id, text='Привет!')


def error(bot, update, error):
    click.secho('Update "{0}" caused error "{1}"'.format(update, error), fg='red')


def run_bot(bot_token, vins_app_id, vins_url, vins_client_token):
    vins_api = VinsAPI(vins_url, vins_app_id, vins_client_token)

    # Create the EventHandler and pass it your bot's token.
    updater = Updater(bot_token, job_queue_tick_interval=0.1, workers=1)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler([lambda x: True], vins_api.bot_handler))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    click.secho('Bot started')
    # Run the bot until the you presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


def main(bot_token):
    with open("Vinsfile.js", "r") as f:
        cfg = commentjson.load(f)
        # - checking app section -
        if cfg.get('tokens') is None:
            raise VinsCfgParseError('"tokens" section is missing')
        token_client = cfg['tokens'].get('client')
        vins_url = cfg.get('vins_url')
        app_id = cfg.get('app_id')
        if token_client is None:
            raise VinsCfgParseError('"client token" is missing')
        if vins_url is None:
            raise VinsCfgParseError('"vins_url" is missing')
        if app_id is None:
            raise VinsCfgParseError('"app_id" is missing')

        run_bot(bot_token, app_id, vins_url + '/', token_client)
