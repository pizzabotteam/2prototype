{
  "name": "order",
  "intents": [
  {
    "intent": "order",
    "nlu": {
      "path": "intents/order.nlu"
    },
    "dm": {
      "path": "intents/order.js"
    },
    "nlg": {
      "path": "intents/order.nlg"
    }    
  }, {
    "intent": "menu",
    "nlu": {
      "path": "intents/menu.nlu"
    },
    "dm": {
      "path": "intents/menu.js"
    },
    "nlg": {
      "path": "intents/menu.nlg"
    }    
  }, {
    "intent": "confirm",
    "nlu": {
      "path": "intents/confirm.nlu"
    },
    "dm": {
      "path": "intents/confirm.js"
    },
    "nlg": {
      "path": "intents/confirm.nlg"
    }
  }, {
    "intent": "not_confirm",
    "nlu": {
      "path": "intents/not_confirm.nlu"
    },
    "dm": {
      "path": "intents/not_confirm.js"
    },
    "nlg": {
      "path": "intents/not_confirm.nlg"
    }
  }, {
    "intent": "item0",
    "nlu": {
      "path": "intents/items/item0.nlu"
    },
    "dm": {
      "path": "intents/items/item0.js"
    },
    "nlg": {
      "path": "intents/items/item0.nlg"
    }
  }, {
    "intent": "item1",
    "nlu": {
      "path": "intents/items/item1.nlu"
    },
    "dm": {
      "path": "intents/items/item1.js"
    },
    "nlg": {
      "path": "intents/items/item1.nlg"
    }      
  }]
}
