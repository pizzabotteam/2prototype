{
  "form": "not_confirm",
  "events": [{
    "event": "submit",
    "handlers": [{
      "handler": "nlg",
      "phrase_id": "ask__me"
    },{
      "handler": "nlg",
      "phrase_id": "ask__not_expected"
    },{
      "handler": "nlg",
      "phrase_id": "ask__not_confirm_add"
    },{
      "handler": "nlg",
      "phrase_id": "ask__start_order"
    },{
      "handler": "nlg",
      "phrase_id": "ask__your_order"
    },{
      "handler": "nlg",
      "phrase_id": "ask__is_confirm_order"
    },{
      "handler": "nlg",
      "phrase_id": "ask__not_confirm_order"
    }]
  }],
  "slots": []
}
