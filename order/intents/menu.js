{
  "form": "menu",
  "events": [{
    "event": "submit",
    "handlers": [{
      "handler": "nlg",
      "phrase_id": "ask__me"
    },{
      "handler": "nlg",
      "phrase_id": "ask__not_expected"
    },{
      "handler": "nlg",
      "phrase_id": "ask__show"
    }]
  }],
  "slots": []
}
