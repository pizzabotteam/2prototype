{
  "form": "confirm",
  "events": [{
    "event": "submit",
    "handlers": [{
      "handler": "nlg",
      "phrase_id": "ask__me"
    },{
      "handler": "nlg",
      "phrase_id": "ask__not_expected"
    },{
      "handler": "nlg",
      "phrase_id": "ask__confirm_add"
    },{
      "handler": "nlg",
      "phrase_id": "ask__is_all"
    },{
      "handler": "nlg",
      "phrase_id": "ask__start_order"
    },{
      "handler": "nlg",
      "phrase_id": "ask__confirm_order"
    }]
  }],
  "slots": []
}
