{
  "form": "item0",
  "events": [{
    "event": "submit",
    "handlers": [{
      "handler": "nlg",
      "phrase_id": "ask__me"
    },{
      "handler": "nlg",
      "phrase_id": "ask__not_expected"
    },{
      "handler": "nlg",
      "phrase_id": "ask__find"
    },{
      "handler": "nlg",
      "phrase_id": "ask__is_confirm"
    },{
      "handler": "nlg",
      "phrase_id": "ask__id"
    }]
  }],
  "slots": []
}
